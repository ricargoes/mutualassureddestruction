extends Area2D

var player_owner = 1

func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		hit(body)

func _on_AnimatedSprite_animation_finished():
	queue_free()


func hit(body):
	if body.get_parent().has_method("damage"):
		body.get_parent().damage(1.0/36)


func _on_Delay_timeout():
	set_physics_process(true)
	$AnimatedSprite.play()
	$AnimatedSprite.show()
	$CollisionShape2D.disabled = false
