extends CanvasLayer

func _ready():
	var players = get_tree().get_nodes_in_group("players")
	for player in players:
		update_scores(player)
		player.connect("life_changed", self, "update_scores")
	Globals.ui = self
	$HBoxContainer/Player1.set_modulate(Globals.COLORS[1])
	$HBoxContainer/Player2.set_modulate(Globals.COLORS[2])


func update_scores(player):
	$HBoxContainer.get_node("Player"+str(player.player_n)+"Score").text = "%3.2f" % player.life
