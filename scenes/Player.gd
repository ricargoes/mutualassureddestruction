extends Node

export var player_n = 1

const TANK_BASE_SPEED = 200
var _tank_speed_dir = Vector2(0,0)
const AIM_BASE_SPEED = 260
var _aim_speed_dir = Vector2(0,0)
var _attacking = false

var life = 10.0

signal life_changed


func _ready():
	set_physics_process(true)
	$Tank/Details.set_modulate(Globals.COLORS[player_n])
	$Aim/Crossair.set_modulate(Globals.COLORS[player_n])


func _physics_process(delta):
	player_input()
	
	if _attacking:
		try_attack()
	tank_movement(delta)
	aim_movement(delta)


func tank_movement(delta):
	$Tank.move_and_collide(_tank_speed_dir*TANK_BASE_SPEED*delta)
	if _tank_speed_dir != Vector2(0, 0):
		$Tank.set_rotation(_tank_speed_dir.angle() + PI/2)


func aim_movement(delta):
	var final_pos = $Aim.get_global_position()+_aim_speed_dir*AIM_BASE_SPEED*delta
	var x = max( 0, min(final_pos.x, ProjectSettings.get_setting("display/window/size/width")) )
	var y = max( 0, min(final_pos.y, ProjectSettings.get_setting("display/window/size/height")) )
	$Aim.set_global_position(Vector2(x, y))
	$Tank/Turret.set_global_rotation(($Aim.get_position() - $Tank.get_position()).angle() + PI/2)

func player_input():
	var tank_joystick = Vector2(Input.get_joy_axis(player_n - 1, JOY_AXIS_0), Input.get_joy_axis(player_n - 1, JOY_AXIS_1))
	if tank_joystick.length() > 0.2:
		_tank_speed_dir = tank_joystick
	else:
		_tank_speed_dir = Vector2(0, 0)

	var aim_joystick = Vector2(Input.get_joy_axis(player_n - 1, JOY_AXIS_2), Input.get_joy_axis(player_n - 1, JOY_AXIS_3))
	if aim_joystick.length() > 0.2:
		_aim_speed_dir = aim_joystick
	else:
		_aim_speed_dir = Vector2(0, 0)
	
	if Input.is_action_pressed(str(player_n) + "_attack"):
		_attacking = true
	else:
		_attacking = false


func try_attack():
	if $AttackCooldown.is_stopped():
		var impact = Globals.impact_class.instance()
		impact.set_global_position($Aim.get_global_position())
		impact.player_owner = player_n
		get_parent().add_child(impact)
		$AttackCooldown.start()


func damage(amount):
	life = max(0, life - amount)
	emit_signal("life_changed", self)
	if life <= 0:
		die()


func die():
	queue_free()
